import React from 'react'
import styled from 'styled-components'

const Img = styled.img`
  width: 250px;
  height: 200px;
`

export const Image = ({ src }) => <Img src={src} />