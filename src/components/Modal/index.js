import React from 'react'
import styled, { keyframes } from 'styled-components'

const animateTop = keyframes`
  from {top: -50%; opacity: 0}
  to {top: 50%; opacity: 1}
`

const ModalContainer = styled.div`
  position: fixed;
  top: 0;
  z-index: 100;
`

const ModalBackground = styled.div`
  background: black;
  opacity: 0.5;
  width: 100vw;
  height: 100vh;
`

const ModalCard = styled.div`
  position: absolute;
  z-index: 100;
  background: white;
  width: 70%;
  padding: 20px;
  margin-left: calc(15% - 20px);
  top: 50%;
  transform: translateY(-50%);  
  display: flex;  
  flex-direction: column;
  align-items: center;
  animation-name: ${animateTop};
  animation-duration: 0.5s;

  div {
    margin-top: 15px;
  }

  img {
    width: 200px;
  }

  span {
    font-weight: bold;
    margin-right: 15px;
  }
`

const ModalHeader = styled.div`
  margin-bottom: 30px;
  font-size: 20px;
  font-weight: 600;

  span {
    position: absolute;
    right: 15px;
    font-size: 30px;
    cursor: pointer;
  }
`

export const Modal = React.memo(({ children, open, title, closeModal }) => (
  open ?
    <ModalContainer>
      <ModalBackground></ModalBackground>
      <ModalCard>
        <ModalHeader>
          {title}
          <span onClick={() => closeModal()} > X </span>
        </ModalHeader>
        {children}
      </ModalCard>
    </ModalContainer> : null
))