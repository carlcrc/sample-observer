import React, { useState, useEffect } from 'react'
import { sectionsMock } from './sections.mock'

import { subsectionModel } from './models/Subsection'
import { sectionsObserver } from './models/Sections'

import { Subsection } from './Subsection.component'

import { Preview } from './Preview.component'

export const Sections = () => {
  const [sections, setSections] = useState({})


  function sectionsUpdated(newState) {
    const { sections } = newState
    setSections(sections)
  }

  useEffect(() => {
    sectionsObserver.subject.attach(sectionsUpdated)

    return () => sectionsObserver.subject.detach(sectionsUpdated)
  }, [])

  useEffect(() => {
    sectionsObserver.setInitialSections(sectionsMock)
  },[])

  return(
    <div style={{ display: 'flex' }}>
    <div>
        <h1>Sections</h1>
        {Array.isArray(sections._sectionsOrder) && sections?._sectionsOrder.map(key => {
          const { title, predefinedSectionId, subsections, id } =  sections[key]
          return (
            <h2
              key={id}
              onClick={() => {
                subsectionModel.setProdefinedSectionId(predefinedSectionId)
                subsectionModel.setSubsection(subsections)
              }}
            >
              {title}
            </h2>
          )
        })}
      </div>
      <Subsection />
      <Preview />
    </div>
  )
}