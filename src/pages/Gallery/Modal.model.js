import { Subject } from 'subject'

class Modal {
  constructor(subject) {
    this.subject = subject
    this.open = false
    this.photo = null
  }

  closeModal = () => {
    this.open = false
    this.subject.notify(this)
  }

  openModal = () => {
    this.open = true
    this.subject.notify(this)
  }

  setContentToShow = (content) => {
    this.content = content
    this.subject.notify(this)
  }
}

const modal = new Modal(new Subject)

export {
  modal
}