import uniqid from 'uniqid'
import { Subject } from 'subject'
import { Field } from './Field'


export class Sections {
  constructor(subject) {
    this.subject = subject
    this.sections = {}
  }

  setInitialSections(sections) {
    sections._sectionsOrder.forEach(key => {
      sections[key]['id'] = uniqid()
      sections[key].subsections.forEach(subsection => {
        subsection['id'] = uniqid()
        subsection.fields = Object.fromEntries(
          Object.entries(subsection.fields)
          .map(([ key, field ]) => [ key, new Field(field) ])
        );
      })
    });
    this.sections = sections
    this.subject.notify(this);
  }

}

export const sectionsObserver = new Sections(new Subject)

