import React, { useState, useEffect } from 'react'
import { useForceRerender } from './../hooks'

export const Input = ({ field }) => {
  const forceRenderer = useForceRerender();
  // const [text, setText] = useState('')

  // function fieldModelUpdated(newState) {
  //   const { value } = newState
  //   setText(value)
  // }

  // useEffect(() => {
  //   field.attach(fieldModelUpdated)

  //   return () => field.detach(fieldModelUpdated)
  // }, [])

  return (
    <div>
      <input
        defaultValue={field.value}
        placeholder={field.placeholder}
        onChange={(e) => {
          field.onChangeHandlerValue(e.target.value)
          forceRenderer()
        }}
      />
    </div>
  );
}