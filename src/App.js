import React from 'react'

import { Sections } from 'pages/Sections'

export function App() {

  return (
    <Sections />
  );
}