import React, { useEffect, useState } from 'react'

import { subsectionModel } from './models/Subsection'
import { Input } from './components/Input'

export const Subsection = () => {
  const [subsection, setSubsection] = useState(null);
  
  const subsecionUpdated = (newState) => {
    const { subsection } = newState
    setSubsection(subsection)
  }

  useEffect(() => {
    subsectionModel.subject.attach(subsecionUpdated)

    return () => subsectionModel.subject.detach(subsecionUpdated)
  },[])

  return (
    <div>
      {subsection && Object.values(subsection?.fields)
      .map((field) => <Input key={field.id} field={field}/>)}
    </div>
  )
}