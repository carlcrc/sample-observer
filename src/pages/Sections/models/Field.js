import uniqid from 'uniqid'
import { Subject } from 'subject'


export class Field extends Subject {
  constructor(field) {
    super()
    this.id = uniqid()
    this.label = field.label
    this.options = field.options
    this.placeholder = field.placeholder
    this.templateTag = field.templateTag
    this.type = field.type
    this.value = field.value
  }

  onChangeHandlerValue = (newValue) => {
    this.value = newValue
    this.notify(this)
  }
}