import React, { useEffect, useState } from 'react'

import { Modal } from 'components/Modal'
import { Image } from 'components/Image'

import { modal as modalModel } from './Modal.model'

export const ModalContent = () => {
  const [open, setOpen] = useState(modalModel.open)
  const [photo, setPhoto] = useState(modalModel.photo)

  function updateModal(newState) {
    const { content, open } = newState
    setPhoto(content)
    setOpen(open)
  }

  function updatePhoto(newState) {
    setPhoto({ ...newState })
  }

  useEffect(() => {
    modalModel?.subject?.attach(updateModal)

    return () => modalModel?.subject?.detach(updateModal)
  }, [])

  useEffect(() => {
    photo?.attach(updatePhoto)

    return () => photo?.detach(updatePhoto)
  }, [photo])

  return (
    <Modal
      title={photo?.title}
      open={open}
      closeModal={modalModel.closeModal}
    >
      <Image src={photo?.imageURL} />
      <p>{photo?.user}</p>
      <p>{photo?.likes}</p>
      <input onChange={photo?.onChangeTitle} />
    </Modal>
  )
}