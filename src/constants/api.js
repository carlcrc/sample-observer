
const apiUrl = `https://pixabay.com/api/?key=${process.env.API_KEY}`

export {
  apiUrl
}