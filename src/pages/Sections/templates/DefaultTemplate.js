import React, { useEffect } from 'react'

import { useForceRerender } from './../hooks'

export const DefaultTemplate = ({ field }) => {
  const forceRenderer = useForceRerender();

  function fieldUpdated() {
    forceRenderer()
  }

  useEffect(() => {
    field.attach(fieldUpdated)

    return () => field.detach(fieldUpdated)
  }, [])

  return (
    <span>
      {field.value 
        ? field.value
        : field.placeholder
      }
    </span>
  )
}