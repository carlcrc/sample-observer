import { apiUrl } from 'constants/api'
import { Fetcher } from 'helpers/Fetcher'

export const getImages = () => {
  return Fetcher.get(`${apiUrl}`)
}