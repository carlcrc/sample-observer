import { errorHandler } from 'decorators'

const parseJSON = response => response.json()

export class Fetcher {

  @errorHandler
  static get(path, customOptions) {
    return fetch(path, {
      method: 'GET',
      ...customOptions
    })
      .then(parseJSON)
  }
}