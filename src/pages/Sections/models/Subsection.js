import { Subject } from 'subject'

class Subsection {
  constructor(subject) {
    this.subject = subject
    this.predefinedSectionId = ''
    this.subsection = {}
  }

  setProdefinedSectionId(predefinedSectionId) {
    this.predefinedSectionId = predefinedSectionId
  }

  setSubsection(subsections) {
    this.subsection = subsections.find(({ predefinedSectionId }) => 
      predefinedSectionId === this.predefinedSectionId)
    this.subject.notify(this)
  }
}

const subsectionModel = new Subsection(new Subject)

export {
  subsectionModel
}