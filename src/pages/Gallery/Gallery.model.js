import { Subject } from 'subject'
import { Photo } from './Photo.model'

class Gallery {
  constructor(subject) {
    this.subject = subject
    this.gallery = []
  }

  setGallery = (gallery) => {
    this.gallery = gallery.map(photo => new Photo(photo))
    this.subject.notify(this)
  }

}

const gallery = new Gallery(new Subject)

export {
  gallery
}