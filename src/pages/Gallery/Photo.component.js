import React, { useState, useEffect } from 'react'

import { Image } from 'components/Image'
import { Button } from 'components/Button'

import { modal as modalModel } from './Modal.model'

export const Photo = ({ attach, detach, imageURL, ...rest }) => {
  const [title, setTitle] = useState(title)

  function photoModelUpdated(newState) {
    const { title } = newState
    setTitle(title)
  }

  useEffect(() => {
    attach(photoModelUpdated)

    return () => detach(photoModelUpdated)
  }, [])

  return (
    <div>
      <Image src={imageURL} />
      <h3>{title}</h3>
      <Button
        onClick={() => {
          modalModel.setContentToShow({ attach, detach, imageURL, ...rest })
          modalModel.openModal()
        }}
      >
        Show Info
      </Button>
    </div>
  )
}
