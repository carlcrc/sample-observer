import { Subject } from 'subject'

export class Photo extends Subject {
  constructor({ id, imageURL, largeImageURL, likes, user, tags }) {
    super()
    this.title = ''
    this.imageURL = imageURL
    this.largeImageURL = largeImageURL
    this.id = id
    this.likes = likes
    this.user = user
    this.tags = tags
  }

  onChangeTitle = (event) => {
    this.title = event.target.value
    this.notify(this)
  }
}