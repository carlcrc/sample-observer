import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

import { Title } from 'components/Title'

import { ModalContent } from './Modal.component'
import { Photo } from './Photo.component'

import { gallery as galleryModel } from './Gallery.model'

import { getImages } from 'services/getImages';

const GalleryContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
`

export function Gallery() {
  const [gallery, setGallery] = useState(galleryModel.gallery)
  const [error, setError] = useState('')

  async function fetchImages() {
    const [error, response] = await getImages()
    if (error) {
      return setError(error)
    }
    galleryModel.setGallery(response.hits);
  }

  function galleryModelUpdated(newState) {
    const { gallery } = newState
    setGallery(gallery)
  }

  useEffect(() => {
    galleryModel.subject.attach(galleryModelUpdated)

    return () => galleryModel.subject.detach(galleryModelUpdated)
  }, [])

  useEffect(() => {
    fetchImages()
  }, [])

  return (
    <>
      <Title>Gallery</Title>
      <GalleryContainer>
        {gallery.map(photo => <Photo key={photo.id} {...photo} />)}
      </GalleryContainer>
      <ModalContent />
    </>
  );
}