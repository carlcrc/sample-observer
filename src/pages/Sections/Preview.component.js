import React, { useState, useEffect } from 'react'

import { sectionsObserver } from './models/Sections'

import { DefaultTemplate } from './templates'


export const Preview = () => {
  const [sections, setSections] = useState({})

  function sectionsUpdated(newState) {
    const { sections } = newState
    setSections(sections)
  }

  useEffect(() => {
    sectionsObserver.subject.attach(sectionsUpdated)

    return () => sectionsObserver.subject.detach(sectionsUpdated)
  }, [])

  return(
    <div>
      <h1>Preview</h1>
      {Array.isArray(sections._sectionsOrder) && sections?._sectionsOrder.map(key => {
        const { subsections } =  sections[key]
        return (
          subsections.map((subsection) => {
            return (
              <div key={subsection.id} style={{display: 'flex', flexDirection: 'column'}}>
                <h4>
                  {subsection.title}
                </h4>
                {Object.values(subsection.fields)
                  .map(field => <DefaultTemplate key={field.id} field={field}/>)}
              </div>
            )
          }))
      })}
    </div>
  )
}